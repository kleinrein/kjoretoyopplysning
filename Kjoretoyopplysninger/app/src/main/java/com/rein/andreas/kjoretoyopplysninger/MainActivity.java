package com.rein.andreas.kjoretoyopplysninger;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsLayoutInflater;
import com.mikepenz.ionicons_typeface_library.Ionicons;
import com.rein.andreas.kjoretoyopplysninger.adapters.OpplysningerAdapter;
import com.rein.andreas.kjoretoyopplysninger.data.TinyDB;
import com.rein.andreas.kjoretoyopplysninger.models.Kjoretoyopplysning;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    // UI
    @BindView(R.id.clMain)
    ConstraintLayout clMain;
    @BindView(R.id.edtRegNr)
    EditText edtRegNr;
    @BindView(R.id.cmdSøk)
    Button cmdSøk;
    @BindView(R.id.rvOpplysninger)
    RecyclerView rvOpplysninger;
    @BindView(R.id.dl_drawer)
    DrawerLayout dlDrawer;
    @BindView(R.id.lv_history)
    ListView lvHistory;

    // Fields
    private ArrayList<String> history = new ArrayList<>();
    private TinyDB tinyDB;
    private ArrayAdapter<String> historyAdapter;

    // Constants
    private static final int HISTORY_MAX_SIZE = 50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory(getLayoutInflater(), new IconicsLayoutInflater(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Get history
        tinyDB = new TinyDB(getApplicationContext());
        history = tinyDB.getListString("history");

        historyAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, history);
        lvHistory.setAdapter(historyAdapter);
        lvHistory.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemVal = history.get(position);
                getKjoretoyOpplysninger(itemVal);
                dlDrawer.closeDrawer(GravityCompat.END);

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtRegNr.getWindowToken(), 0);
                edtRegNr.setText(itemVal);
            }
        });

        cmdSøk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cmdSokClicked();
            }
        });
    }

    private void cmdSokClicked() {
        if (!isEdtEmpty(edtRegNr)) {
            getKjoretoyOpplysninger(edtRegNr.getText().toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        menu.findItem(R.id.nav_history).setIcon(new IconicsDrawable(this).icon(Ionicons.Icon.ion_android_list).color(Color.WHITE).sizeDp(18));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_history:
                dlDrawer.openDrawer(GravityCompat.END);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getKjoretoyOpplysninger(String regNr) {
        // regNr = "vf15886";

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://www.vegvesen.no/Kjoretoy/Kjop+og+salg/Kj%C3%B8ret%C3%B8yopplysninger?registreringsnummer=" + regNr, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.i("testing", "onFailure: " + responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                try {
                    LinkedList<String> titles = new LinkedList<>();
                    LinkedList<String> content = new LinkedList<>();

                    Document doc = Jsoup.parse(responseString);

                    Elements tables = doc.getElementsByClass("trekkspill-definisjonsliste");
                    for (Element table : tables) {

                        String title = table.getElementsByClass("tittel").first().text();
                        titles.add(title);
                        content.add("");

                        Element parent = table.getElementsByClass("definisjonsliste").first();

                        for (Element children : parent.children()) {
                            if (children.tagName() == "dt") {
                                titles.add(children.text());
                            } else {
                                content.add(children.text());
                            }
                        }
                    }

                    Log.i("testing", "content: " + content.toString());

                    if (content.size() != 0) {
                        history.add(0, regNr);
                        historyAdapter.notifyDataSetChanged();
                        saveHistory();
                        visOpplysning(new Kjoretoyopplysning(titles, content));
                    } else {
                        showSnackbar(R.string.error_regnr_not_found);
                    }
                } catch (Exception e) {
                    // TODO
                    // show error
                    Log.i("testing", "onSuccess exception: " + e.toString());
                }
            }
        });
    }

    private void saveHistory() {
        if (history.size() > HISTORY_MAX_SIZE) {
            // Delete last item
            history.remove(history.size());
        }

        tinyDB.putListString("history", history);
    }

    private void showSnackbar(int str) {
        Snackbar snackbar = Snackbar.make(clMain, str, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    private void visOpplysning(Kjoretoyopplysning kjoretoyopplysning){
        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.this);
        rvOpplysninger.setLayoutManager(llm);
        OpplysningerAdapter adapter = new OpplysningerAdapter(kjoretoyopplysning, getApplicationContext());
        rvOpplysninger.setAdapter(adapter);
    }

    private boolean isEdtEmpty(EditText editText) {
        return editText.getText().toString().equals("") || editText.getText().length() <= 0;
    }
}
