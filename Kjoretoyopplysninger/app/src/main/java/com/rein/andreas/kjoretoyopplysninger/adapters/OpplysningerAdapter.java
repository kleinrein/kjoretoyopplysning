package com.rein.andreas.kjoretoyopplysninger.adapters;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rein.andreas.kjoretoyopplysninger.R;
import com.rein.andreas.kjoretoyopplysninger.models.Kjoretoyopplysning;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Andreas on 07/25/2016.
 */

public class OpplysningerAdapter extends RecyclerView.Adapter<OpplysningerAdapter.OpplysningerViewHolder> {
    private Kjoretoyopplysning kjoretoyopplysning;
    private Context context;

    public OpplysningerAdapter(Kjoretoyopplysning kjoretoyopplysning, Context context) {
        this.kjoretoyopplysning = kjoretoyopplysning;
        this.context = context;
    }

    static class OpplysningerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtContent)
        TextView txtContent;
        @BindView(R.id.v_bottom_border)
        View vBottomBorder;

        OpplysningerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemCount() {
        return kjoretoyopplysning.getSize();
    }

    @Override
    public void onBindViewHolder(OpplysningerViewHolder holder, int position) {
        if (kjoretoyopplysning.getContent().get(position).equals("")) {
            holder.txtTitle.setAllCaps(true);
            holder.txtTitle.setTextSize(22f);
            holder.vBottomBorder.setVisibility(View.GONE);
        } else {
            holder.txtTitle.setAllCaps(false);
            holder.txtTitle.setTextSize(16f);
        }

        holder.txtTitle.setText(kjoretoyopplysning.getTitles().get(position));
        holder.txtContent.setText(kjoretoyopplysning.getContent().get(position));
    }

    @Override
    public OpplysningerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.opplysninger_layout, parent, false);
        OpplysningerViewHolder ovh = new OpplysningerViewHolder(v);
        return ovh;
    }
}