package com.rein.andreas.kjoretoyopplysninger.models;

import java.util.LinkedList;

/**
 * Created by Andreas on 07/25/2016.
 */

public class Kjoretoyopplysning {
    private LinkedList<String> titles = new LinkedList<>();
    private LinkedList<String> content = new LinkedList<>();

    public Kjoretoyopplysning(LinkedList<String> titles, LinkedList<String> content) {
        this.titles = titles;
        this.content = content;
    }

    public LinkedList<String> getTitles() {
        return titles;
    }
    public void setTitles(LinkedList<String> titles) {
        this.titles = titles;
    }
    public LinkedList<String> getContent() {
        return content;
    }
    public void setContent(LinkedList<String> content) {
        this.content = content;
    }

    public int getSize(){
        return titles.size();
    }

}
